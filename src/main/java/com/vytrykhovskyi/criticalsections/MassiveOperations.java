package com.vytrykhovskyi.criticalsections;

public class MassiveOperations {
    private int mas[] = new int[MAX_SIZE];
    final static int MAX_SIZE = 10;

    public MassiveOperations(int second) {
        mas[1] = second;
    }

    public void firstMethod() {
        Object ob = new Object();
        //    synchronized (ob) {
        synchronized (this) {
            for (int i = 0; i < MAX_SIZE; i++) {
                mas[i] = i;
            }
            System.out.print(mas[1]);
            System.out.println(" - This is the second element of array " + Thread.currentThread().getName());

        }
    }

    public void secondMethod() {
        Object ob1 = new Object();
        //      synchronized  (ob1) {
        synchronized (this) {
            for (int i = 0; i < MAX_SIZE; i++) {
                mas[i] *= 2;
            }
            System.out.print(mas[1]);
            System.out.println(" - This is the second element of array " + Thread.currentThread().getName());
        }
    }

    public void thirdMethod() {
        Object ob2 = new Object();
        //  synchronized (ob2) {
        synchronized (this) {
            for (int i = 0; i < MAX_SIZE; i++) {
                mas[i] *= 10;
            }
            System.out.print(mas[1]);
            System.out.println(" - This is the second element of array " + Thread.currentThread().getName());
        }
    }
}
