package com.vytrykhovskyi;

import java.util.Random;

public class Randomize {
    static Random r = new Random();

    public static int getRandomNumber(int start, int finish) {
        return r.nextInt((finish - start + 1) + start);
    }
}
