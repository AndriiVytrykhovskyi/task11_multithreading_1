package com.vytrykhovskyi.callable;

import com.vytrykhovskyi.Randomize;
import com.vytrykhovskyi.fibonacci.Fibonacci;
import java.util.List;
import java.util.concurrent.Callable;

public class SumCallable implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        List<Integer> fib = new Fibonacci(Randomize.getRandomNumber(0,10)).getFibonacci();
        System.out.println(Thread.currentThread().getName());
        System.out.println(fib);
        return fib.stream().mapToInt(Integer::intValue).sum();
    }
}
