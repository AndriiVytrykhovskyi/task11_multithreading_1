package com.vytrykhovskyi.callable;

import java.util.concurrent.*;

public class App {
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(3);

        Future<Integer> futureA = service.submit(new SumCallable());
        Future<Integer> futureB = service.submit(new SumCallable());
        Future<Integer> futureC = service.submit(new SumCallable());

        try {
            System.out.println("Sum = " + futureA.get());
            System.out.println("Sum = " + futureB.get());
            System.out.println("Sum = " + futureC.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        service.shutdown();
    }
}
