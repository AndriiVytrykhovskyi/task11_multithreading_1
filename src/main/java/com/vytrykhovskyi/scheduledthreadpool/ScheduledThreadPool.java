package com.vytrykhovskyi.scheduledthreadpool;

import com.vytrykhovskyi.Randomize;

import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledThreadPool {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int threadCount = sc.nextInt();
        ScheduledExecutorService scheduledThreadPool =
                Executors.newScheduledThreadPool(threadCount);
        try {
            for (int i = 1; i <= threadCount; i++) {
                MyThread myThread = new MyThread("thread " + i);
                scheduledThreadPool.schedule(myThread, Randomize.getRandomNumber(0, 10), TimeUnit.SECONDS);
            }
        } finally {
            scheduledThreadPool.shutdown();
        }
    }
}
