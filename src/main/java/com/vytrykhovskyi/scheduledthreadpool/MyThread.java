package com.vytrykhovskyi.scheduledthreadpool;

import java.util.Date;

public class MyThread implements Runnable {
    private String name;

    public MyThread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            System.out.println("It`s " + name + " - Time - " + new Date());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
