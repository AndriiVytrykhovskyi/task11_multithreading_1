package com.vytrykhovskyi.fibonacci;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci extends Thread {
    private List<Integer> fibonacci = new ArrayList<>();
    private int N;

    public Fibonacci(int n) {
        N = n;
        createFibonacciRow();
    }

    public void createFibonacciRow() {
        fibonacci.add(1);
        fibonacci.add(1);
        for (int i = 2; i < N; i++) {
            fibonacci.add(fibonacci.get(i - 2) + fibonacci.get(i - 1));
        }
    }

    public List<Integer> getFibonacci() {
        return fibonacci;
    }

    public void run() {
        System.out.println(currentThread().getName());
        System.out.println(fibonacci);
    }
}
