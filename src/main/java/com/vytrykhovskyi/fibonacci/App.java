package com.vytrykhovskyi.fibonacci;

import com.vytrykhovskyi.Randomize;

public class App {
    public static void main(String[] args) {

        for (int i = 0; i < 10 ; i++) {
            new Fibonacci(Randomize.getRandomNumber(0,15)).start();
        }
    }
}
