package com.vytrykhovskyi.pingpong;

public class Player extends Thread {
    private Table t;
    private String name;
    private int count = 0;

    public Player(Table t, String name) {
        this.name = name;
        this.t = t;
        start();
    }

    public void run() {
        if(name.equals("Ping")){
            try {
                while (true){
                    t.pingTurn();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(name.equals("Pong")){
            try {
                while (true){
                    t.pongTurn();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
