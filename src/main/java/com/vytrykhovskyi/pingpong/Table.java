package com.vytrykhovskyi.pingpong;

public class Table {
    private String playerTurn;

    synchronized void pingTurn() throws InterruptedException {
        System.out.println("Ping");
        notify();
        wait();

    }

    synchronized void pongTurn() throws InterruptedException {
        System.out.println("Pong");
        notify();
        wait();

    }
}
