package com.vytrykhovskyi.executors;

import com.vytrykhovskyi.Randomize;
import com.vytrykhovskyi.fibonacci.Fibonacci;

import java.util.concurrent.*;

public class FibonacciExecutor {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        ScheduledExecutorService executorService1 = Executors.newSingleThreadScheduledExecutor();

        try {
            executorService.submit(new Fibonacci(Randomize.getRandomNumber(0, 10)));
            executorService1.schedule(new Fibonacci(Randomize.getRandomNumber(0, 10)), 4, TimeUnit.SECONDS);
        } finally {
            executorService.shutdown();
            executorService1.shutdown();
        }

    }
}
